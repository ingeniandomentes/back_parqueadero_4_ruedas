<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    // Ruta de inicio de sesión
    Route::post('login', 'AuthController@login');

    Route::group(['middleware' => 'auth:api'], function() {
        // Rutas de cierre de seisón y creación de usuarios
        Route::post('signup', 'AuthController@signup');
        Route::get('logout', 'AuthController@logout');

        // Rutas de consulta para el parqueadero
        Route::post('parqueadero_info', 'ParqueaderoController@index');
        Route::post('parqueadero', 'ParqueaderoController@store');
        Route::get('parqueadero/vehiculos', 'ParqueaderoController@getVehicles');
    });
});
