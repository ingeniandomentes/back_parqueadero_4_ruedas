<p align="center"><img src="https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.gradiweb.com%2F&psig=AOvVaw3xmHEFWVKnObM_shxLbsf5&ust=1597250719482000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCPiVr-HMk-sCFQAAAAAdAAAAABAD" width="400"></p>

# PASOS PARA INSTALACIÓN DE LARAVEL BACk PARQUEADERO 4 RUEDAS

**Repositorio**

En primera instancia debes ir al repositorio publico, dar en la opción <button style="background-color:#337AB7;border-radius: 20px;">Clone</button> y escoger la opción HTTPS o si prefieres puedes copiarlo de aquí:
```sh
$ git clone <url> .
```
**Aclaración**:
La anotación "." al final del comando indica que en la carpeta donde estas ejecutando el comando se va a "descomprimir" todo el proyecto, si deseas que se genere una nueva carpeta para el proyecto, unicamente debes quitar este ".".

Este comando debe ser ejecutado en una terminal, donde deseemos guardar nuestro poryecto.

**Instalación de dependencias**

Una vez termine de clonar, debes correr la siguiente linea de comandos:
```sh
$ composer install
```
Una vez finalizada la instalación de composer, nos dirigimos a la raiz del proyecto y buscamos el archivo .env.example, lo copiamos en la misma ubicación y modificamos los siguientes parametros los cuales nos permitiran realizar la conexíon con la base de datos:
```sh
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=parqueadero // Puede usar esta base o cualquiera que guste
DB_USERNAME= <Usuario de la base de datos>
DB_PASSWORD= <Contraseña del usuario de la base de datos>
```
Una vez cambiadas estas opciones en el archivo .env, debemos dirigirnos nuevamente a la consola y debemos correr el siguiente comando:
```sh
$ php artisan key:generate
```
Con estas llave ya podremos realizar consultas a la base de datos y en laravel.

**Creación de base de datos**

Ahora, ya cambiados los valores de la base de datos y generada la llave del proyecto, debemos correr las migraciones, para ello ejecutamos el siguiente comando:
```sh
$ php artisan migrate
```
Este proceso puede tartar unos segundos, esto acorde a cada computador.

Una vez se ejecuten todas las migraciones, podremos validar en nuestra base de datos que se han generado todas las tablas para el proyecto.

Para culminar, correremos los seeders para ingresar información basica a nuestras tablas para poder empezar a usar nuestro sistema, para ello correremos el siguiente comando:
```sh
$ php artisan db:seed
```

Ya puedes empezar a utilizar el sistema.

**¡Diseñado y desarrollado por Ricardo Franco!**

<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[Many](https://www.many.co.uk)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- [UserInsights](https://userinsights.com)
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)
- [User10](https://user10.com)
- [Soumettre.fr](https://soumettre.fr/)
- [CodeBrisk](https://codebrisk.com)
- [1Forge](https://1forge.com)
- [TECPRESSO](https://tecpresso.co.jp/)
- [Runtime Converter](http://runtimeconverter.com/)
- [WebL'Agence](https://weblagence.com/)
- [Invoice Ninja](https://www.invoiceninja.com)
- [iMi digital](https://www.imi-digital.de/)
- [Earthlink](https://www.earthlink.ro/)
- [Steadfast Collective](https://steadfastcollective.com/)
- [We Are The Robots Inc.](https://watr.mx/)
- [Understand.io](https://www.understand.io/)
- [Abdel Elrafa](https://abdelelrafa.com)
- [Hyper Host](https://hyper.host)
- [Appoly](https://www.appoly.co.uk)
- [OP.GG](https://op.gg)
- [云软科技](http://www.yunruan.ltd/)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
