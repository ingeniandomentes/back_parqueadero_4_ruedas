<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Propietario;
use App\Vehiculo;

class ParqueaderoController extends Controller
{
    // Consulta todos los propietarios y vehiculos por marca, placa y/o nombre, opcionales.
    public function index(Request $request)
    {
        $query = '';

        $query = Propietario::query();

        $query->select(
            'propietarios.id as id',
            'propietarios.nombre as nombre',
            'propietarios.cedula as cedula',
            'propietarios.telefono as telefono',
            'propietarios.tiempo_parqueo as tiempo',
            'propietarios.created_at as creado',
            'propietarios.updated_at as actualizado',
            'vehiculos.id as id_vehiculo',
            'vehiculos.marca as marca',
            'vehiculos.modelo as modelo',
            'vehiculos.placa as placa',
            'vehiculos.tipo as tipo'
        );

        $query->join('vehiculos', 'vehiculos.id_propietario_fk','=','propietarios.id');

        if($request->marca != null) {
            $query->where('marca', $request->marca);
        }

        if($request->placa != null) {
            $query->where('placa', $request->placa);
        }

        if($request->nombre != null) {
            $query->where('nombre','like', '%'.$request->nombre.'%');
        }

        if($request->cedula != null) {
            $query->where('cedula', $request->cedula);
        }

        return response()->json($query->get());
    }

    // Almacena los nuevos carros y los nuevos clientes
    public function store(Request $request)
    {
        $request->validate([
            'marca' => 'required|string',
            'modelo' => 'required|string',
            'placa' => 'required|string',
            'tipo' => 'required|string'
        ]);

        if($request->typeUser == 0){
            $request->validate([
                'nombre'     => 'required|string',
                'cedula'    => 'required|numeric|min:10',
                'telefono' => 'required|string|min:7',
                'tiempo_parqueo' => 'required|numeric'
            ]);

            $propietario = new Propietario([
                'nombre'     => $request->nombre,
                'cedula'    => $request->cedula,
                'telefono' =>$request->telefono,
                'tiempo_parqueo' => $request->tiempo_parqueo
            ]);

            $propietario->save();

            $vehiculo = new Vehiculo([
                'marca' => $request->marca,
                'modelo' => $request->modelo,
                'placa' => $request->placa,
                'tipo' => $request->tipo,
                'id_propietario_fk' => $propietario->id,
            ]);

            $vehiculo->save();

            return response()->json([
                'message' => 'Vehiculo y propietario creados exitosamente!'], 201);
        } else {
            $propietario = Propietario::where('id', $request->id_selected_propietario)->first();

            $vehiculo = new Vehiculo([
                'marca' => $request->marca,
                'modelo' => $request->modelo,
                'placa' => $request->placa,
                'tipo' => $request->tipo,
                'id_propietario_fk' => $propietario->id,
            ]);

            $vehiculo->save();

            return response()->json([
                'message' => 'Vehiculo creado exitosamente!'], 201);
        }
    }

    // Consulta la cantidad de vehiculos por marca
    public function getVehicles(){
        $vehiculos = Vehiculo::select('marca')->get();
        $marcas = [];

        foreach($vehiculos as $vehiculo){
            array_push($marcas, $vehiculo->marca);
        }

        $marcas_unicas = array_unique($marcas);
        $marcas_unicas = array_values($marcas_unicas);
        $cuenta_marca = [];

        for($i = 0; $i < count($marcas_unicas); $i++){
            $count = 0;
            foreach($vehiculos as $vehiculo){
                if($vehiculo->marca == $marcas_unicas[$i]){
                    $count = $count + 1;
                }
            }

            $temp = [
                'marca' => Str::ucfirst($marcas_unicas[$i]),
                'cantidad' => $count,
            ];

            array_push($cuenta_marca, $temp);
        }

        return response()->json($cuenta_marca);
    }
}
