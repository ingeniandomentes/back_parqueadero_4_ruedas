<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
use Laravel\Passport\Passport;

class CreateUserTest extends TestCase
{

    use RefreshDatabase;
    /**
     * @test
    */
    public function guest_users_can_not_create_users(){
        $response = $this->post('/api/auth/signup',[
            "name"=>"prueba",
            "email"=>"email@gmail.com",
            "password"=>"123456789",
            "password_confirmation"=>"123456789"
        ],[
            'Accept' => 'application/json',
            'X-Requested-With' => 'XMLHttpRequest'
        ]);

        $response->assertJson(['message' => 'Unauthenticated.']);
    }

    /**
     * @test
    */
    public function an_authenticated_user_can_create_users(){

        $user = factory(User::class)->create();

        Passport::actingAs($user);

        $response = $this->post('/api/auth/signup',[
            "name"=>"prueba",
            "email"=>"email@gmail.com",
            "password"=>"123456789",
            "password_confirmation"=>"123456789"
        ]);

        $response->assertStatus(201);
    }
}
