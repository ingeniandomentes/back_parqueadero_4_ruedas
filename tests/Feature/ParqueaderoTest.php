<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
use Laravel\Passport\Passport;

class ParqueaderoTest extends TestCase
{
    /**
     * @test
    */
    public function an_authenticated_user_can_get_vehicle_quantity(){

        $user = factory(User::class)->create();

        Passport::actingAs($user);

        $response = $this->get('/api/auth/parqueadero/vehiculos');

        $response->assertStatus(200);
    }

    /**
     * @test
    */
    public function an_authenticated_user_can_get_vehicles_and_propietarios(){

        $user = factory(User::class)->create();

        Passport::actingAs($user);

        $response = $this->post('/api/auth/parqueadero_info',[
            "marca"=> "", // Pueden ir vacios
            "placa"=> "", // Pueden ir vacios
            "nombre"=> "", // Pueden ir vacios
            "cedula"=> "" // Pueden ir vacios
        ]);

        $response->assertStatus(200);
    }

    /**
     * @test
    */
    public function an_authenticated_user_can_create_vehicle_and_propietario(){

        $user = factory(User::class)->create();

        Passport::actingAs($user);

        $response = $this->post('/api/auth/parqueadero',[
            "marca" => "toyota",
            "modelo"=>"2019",
            "placa"=> "asd452",
            "tipo"=> "carga",
            "typeUser"=> 0,
            "nombre"=> "prueba",
            "cedula"=> "123456789",
            "telefono"=> "6756543",
            "tiempo_parqueo"=> "2"
        ]);

        $response->assertStatus(201);
    }

    /**
     * @test
    */
    public function an_authenticated_user_can_create_vehicle(){

        $user = factory(User::class)->create();

        Passport::actingAs($user);

        $response = $this->post('/api/auth/parqueadero',[
            "marca" => "toyota",
            "modelo"=>"2009",
            "placa"=> "ABC123",
            "tipo"=> "pasajeros",
            "typeUser"=> 1,
            "id_selected_propietario"=>1
        ]);

        $response->assertStatus(201);
    }
}
